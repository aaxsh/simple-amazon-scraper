from scraper import amazon, date, imageloader
from htmlcreator import htmlcreator
import os

date = date()
products, itemsearch = amazon(date)
imageloader(products, date, itemsearch)
os.chdir("../..")
csvfile = open(f"result {date}.csv", "w+")
csvfile.write("Name; Price; ASIN; Link; Stars; Imagepath")
for product in products:
    csvfile.write(f'{product.name}; {product.price}; {product.asin}; {product.link}; {product.star}; /image/prod{product.id}.png\n')
while True:
    htmlfileuser = input("Do you want to create a html File, which shows the result inculding the images? [Y|N]> ")
    if htmlfileuser.upper() == "Y":
        print("Creating html...")
        htmlcreator(products, date)
        break
    elif htmlfileuser.upper() == "N":
        print("No html created!")
        break
    else:
        print("I don't get it!")
