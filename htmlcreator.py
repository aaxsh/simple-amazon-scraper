import os

def htmlcreator(products, date):
    header = '''<!DOCTYPE html>
	<html>
	<head>
	<title>Amazon Scraper by aaxsh</title>
	<meta charset="utf-8">
    <meta name="description" content="An Amazon Scraper which scrapes the names, prices, stars, links and images of products, without using an API.">
    <meta name="author" content="aaxsh">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    </head>
	<body>
	<div class="container"><h3>Amazon scraper by <a href="https://gitlab.com/aaxsh/">aaxsh</a></h3><a href="https://gitlab.com/aaxsh/simple-amazon-scraper">GITLAB</a><hr><table class="u-full-width"><tr><th>Image</th><th>Name</th><th>Price</th><th>ASIN</th><th>Star</th><th>Link</th></tr>'''
    print("Writing html!")
    os.chdir("html")
    htmlfile = open(f"index{date}.html", "w+", encoding="utf-8")
    htmlfile.write(header)
    for product in products:
        htmlfile.write(f'''
        <tr>
            <td><img class="u-max-full-width" src="../image/{date}/prod{product.id}.png" alt="Image of {product.name}"/></td>
            <td>{product.name}</td>
            <td>{product.price}</td>
            <td>{product.asin}</td>
            <td>{product.star}
            <td><a class="button" href="{product.link}">Link</a></td>''')
    htmlfile.write("""</table></div>
	</body>
	</html>""")
    htmlfile.close()
    print("Done!")
    print("-----------------------------")
    print(f"Your result is save in results/html/index{date}.html !")
