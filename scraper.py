import requests, sys, os, datetime
from bs4 import BeautifulSoup
from fake_useragent import UserAgent

#here you can change the amazon site
COUNTRY = "de"
#every Product is an object
class Product(object):
    def __init__(self):
        pass
    def name(self,name):
        self.name = name
    def price(self, price):
        self.price = price
    def link(self, link):
        self.link = link
    def star(self, star):
        self.star = star
    def id(self, id):
        self.id = id
    def asin(self, asin):
        self.asin = asin
    def image(self, image):
        self.image = image
#creates soup -- function beacause need more soups, because different site
def datasoup(pagenumber, searchitem):
    ua = UserAgent(verify_ssl=False)
    url = requests.get(f"https://www.amazon.{COUNTRY}/s/{searchitem}&page={pagenumber}&keywords={searchitem}", headers={'User-Agent': ua.random})
    data = url.content
    soup = BeautifulSoup(data, 'lxml')
    return soup
#for unique folder and result data name
def date():
    date = datetime.datetime.now()
	#better format for windows
    date = date.strftime("%Y-%m-%d%H%M%S")
    return date
#input to make the main program clear
def useritem():
    while True:
        try:
            useritem = int(input("How much items > "))
            if useritem > 300:
                print("Please take an itemnumber which is below 300!")
            elif useritem <= 0:
                print("Come on you use this program to find some data!")
            else:
                break
        except ValueError:
            print("Please insert a number!")
    return useritem
#scrapes Data
def amazon(date):
    print("Starte Scraper")
    searchitem = input("Insert your search item> ")
    searchitem = searchitem.replace(" ", "+")
    itemsearch = useritem()
    itemnumber = 0
    product_errors = 0
    products = []
    pagenumber = 1
    soup = datasoup(pagenumber, searchitem)
    while True:
        product_error = False
        item = soup.find("li", id=f"result_{itemnumber}")
        if item != None:
            product = Product()
            if item.h2 == None:
                product_error = True
            else:
                product.name(item.h2.text)
            #US and EUR have different classes and styles
            price = item.find("span", {"class":"a-size-base a-color-price s-price a-text-bold"})
            if price == None:
                priceus = item.find("span", {"class": "a-offscreen"})
                if priceus == None:
                    product.price("Not aviable!")
                else:
                    if priceus.text == "[Sponsored]":
                        product_error = True
                    else:
                        product.price(priceus.text)
            else:
                product.price(price.text)
            product.link(item.a["href"])
            image = item.find("img", {"class": "s-access-image cfMarker"})
            if image == None:
                product_error = True
            else:
                product.image(image["src"])
            star = item.find("a", {"class": "a-popover-trigger a-declarative"})
            if star == None:
                product.star("None!")
            else:
                product.star(star.findChild("span").text)
            product.asin(item["data-asin"])
            if product_error == False:
                product.id(itemnumber-product_errors)
                products.append(product)
                sys.stdout.write(f"\rGetting {itemnumber-product_errors+1}/{itemsearch}")
                sys.stdout.flush()
            else:
                product_errors += 1
        else:
            pagenumber += 1
            soup = datasoup(pagenumber, searchitem)
        itemnumber += 1
        if itemnumber - product_errors == itemsearch:
            os.chdir("results/image")
            os.mkdir(str(date))
            print("\nDone!")
            return products, itemsearch
            break
#Downloads the images
def imageloader(products, date, itemsearch):
    print("Loading Images.")
    os.chdir(date)
    for product in products:
        url = product.image
        data = requests.get(url).content
        open(f"prod{product.id}.png", "wb").write(data)
        sys.stdout.write(f"\rGetting Images: {product.id+1}/{itemsearch}")
        sys.stdout.flush()
    print("\nImages done!")
